from django.contrib import admin
from ControlPanel.models import Imp, Room, Noise


class ImpAdmin(admin.ModelAdmin):
    list_display = ('name', 'hwMAC', 'room')
    pass


class RoomAdmin(admin.ModelAdmin):
    list_display = ('name',)
    pass


class NoiseAdmin(admin.ModelAdmin):
    list_display = ('db1', 'db2', 'date', 'imp')
    pass


admin.site.register(Imp, ImpAdmin)
admin.site.register(Room, RoomAdmin)
admin.site.register(Noise, NoiseAdmin)