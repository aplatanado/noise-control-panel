import calendar
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.views import APIView
from rest_framework.response import Response
from models import Noise, Room, Imp
import datetime


class NoiseData(APIView):

    def get(self, request, format=None):
        if "startDate" in request.GET and "startTime" in request.GET and "endDate" in request.GET and "endTime" in request.GET:
            try:
                startTime = datetime.datetime(year=int(request.GET["startDate"].split("-")[0]),
                                              month=int(request.GET["startDate"].split("-")[1]),
                                              day=int(request.GET["startDate"].split("-")[2]),
                                              hour=int(request.GET["startTime"].split(":")[0]),
                                              minute=int(request.GET["startTime"].split(":")[1]))

                endTime = datetime.datetime(year=int(request.GET["endDate"].split("-")[0]),
                                            month=int(request.GET["endDate"].split("-")[1]),
                                            day=int(request.GET["endDate"].split("-")[2]),
                                            hour=int(request.GET["endTime"].split(":")[0]),
                                            minute=int(request.GET["endTime"].split(":")[1]))
                print startTime
                print(endTime)
                data = {}
                for room in Room.objects.all():
                    data[room.slug] = []
                    i = 0
                    for imp in Imp.objects.filter(room=room):
                        data[room.slug].append({"label": "db " + imp.name, "data": []})
                        for noise in Noise.objects.filter(imp=imp,
                                                          date__gt=startTime,
                                                          date__lt=endTime).order_by("date"):
                            data[room.slug][i]["data"].append([calendar.timegm(noise.date.utctimetuple()) * 1000,
                                                               (noise.db2 + noise.db1) / 2])
                        i += 1
            except:
                return Response(status=500)
        else:
        # {"room-slug": [{"label":"room-slug", "data":[db,db]},{}]
            data = {}
            for room in Room.objects.all():
                data[room.slug] = []
                i = 0
                for imp in Imp.objects.filter(room=room):
                    data[room.slug].append({"label": "db " + imp.name, "data": []})
                    for noise in Noise.objects.filter(imp=imp,
                                                      date__gt=datetime.datetime.now() -
                                                               datetime.timedelta(minutes=5)).order_by("date"):
                        data[room.slug][i]["data"].append([calendar.timegm(noise.date.utctimetuple()) * 1000,
                                                           (noise.db2 + noise.db1) / 2])
                    i += 1

        return Response(data)

    def post(self, request, format=None):
        if "imp" in request.POST:
            try:
                imp = Imp.objects.get(hwMAC=request.POST["imp"])
                noise = Noise()
                noise.db1 = request.POST["db1"]
                noise.db2 = request.POST["db2"]
                noise.date = datetime.datetime.now()
                noise.imp = imp
                noise.save()
                if (noise.db1 + noise.db2) / 2 > imp.dbHigh and imp.activated:
                    from django.core.mail import send_mail
                    from NoiseControlPanel import settings
                    send_mail('Exceso de ruido en ' + imp.room.name,
                              str((noise.db1 + noise.db2) / 2) + ' db en sala ' + imp.room.name,
                              'noreply@NoiseControlPanel.etsii.ull.es',
                              settings.CONTACT_MAILS,
                              fail_silently=False)
            except Exception as e:
                # Futuro log de errores
                return Response(status=500)
            return Response()
        return Response(status=500)


class ImpConfig(APIView):

    def get(self, request):
        if "imp" in request.GET:
            conf = None
            try:
                imp = Imp.objects.get(hwMAC=request.GET["imp"])
                conf = {"dbHighLimit": imp.dbHigh,
                        "dbMediumLimit": imp.dbMedium,
                        "dbLowLimit": imp.dbLow,
                        "SLEEP_TIME": imp.sleepTime,
                        "SAMPLE_TIME": imp.samplingTime,
                        "ACTIVATED": imp.activated}
                pass
            except Exception as e:
                return Response(status=500)
            return Response(conf)
        return Response(status=500)

    def post(self, request):
        if "imp" in request.POST:
            try:
                imp = Imp.objects.get(hwMAC=request.POST["imp"])

                if "dbHigh" in request.POST:
                    imp.dbHigh = request.POST["dbHigh"]

                if "dbMedium" in request.POST:
                    imp.dbMedium = request.POST["dbMedium"]

                if "dbLow" in request.POST:
                    imp.dbLow = request.POST["dbLow"]

                if "sleepTime" in request.POST:
                    imp.sleepTime = request.POST["sleepTime"]

                if "samplingTime" in request.POST:
                    imp.samplingTime = request.POST["samplingTime"]

                if "activated" in request.POST:
                    imp.activated = request.POST["activated"]

                imp.save()
                pass
            except Exception as e:
                # Futuro log de errores
                return Response(status=500)
            return Response(status=200)
        return Response(status=500)
