from django.db import models
from django.core.exceptions import ValidationError


def is_a_mac(value):
    try:
        value = str(value)
    except:
        raise ValidationError('%s is not a valid mac' % value)

    if len(value) != 12:
        raise ValidationError('%s is not a valid mac' % value)

    permited = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a',
                'b', 'c', 'd', 'e', 'f', 'A', 'B', 'C', 'D', 'E', 'F']

    for letter in value:
        if letter not in permited:
            raise ValidationError('%s is not a valid mac 4' % value)


class Room(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)
    slug = models.SlugField(unique=True)

    def __unicode__(self):
        return self.name
    pass


class Imp(models.Model):
    name = models.CharField(max_length=255)
    hwMAC = models.CharField("MAC address of the imp in format 0c2a69004a3e",
                             max_length=17, validators=[is_a_mac], unique=True)
    room = models.ForeignKey(Room)
    description = models.TextField(null=True, blank=True)

    dbHigh = models.IntegerField()
    dbMedium = models.IntegerField()
    dbLow = models.IntegerField()

    sleepTime = models.FloatField()
    samplingTime = models.IntegerField(help_text="Tiempo en ms que muestrea el imp")

    activated = models.BooleanField(default=True)

    def __unicode__(self):
        return "%s, %s" % (self.name, self.hwMAC)
    pass

    def formated_mac(self):
        return self.hwMAC.replace(":", "")


class Noise(models.Model):
    imp = models.ForeignKey(Imp)
    db1 = models.FloatField()
    db2 = models.FloatField()
    date = models.DateTimeField()

    def __unicode__(self):
        return "%s db, %s db, %s" % (self.db1, self.db1, self.date)
    pass