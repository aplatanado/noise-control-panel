from django.shortcuts import render, render_to_response, RequestContext
from django.views.generic import TemplateView, View
from django.template import Context
from ControlPanel.models import Room


class GraphicPanelView(View):

    def get(self, request):
        context = Context({"rooms": Room.objects.all()})
        return render_to_response("admin/graphics-panel.html", context)


